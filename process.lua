---@module 'tilemaker.lua'

local bake = require 'bake'

local wiktionary = require 'wiktionary/init'
local wlangs = wiktionary.require 'Module:languages'
wiktionary.require 'Module:uk-translit'
wiktionary.require 'Module:ru-translit'

local ja = wiktionary.require 'Module:ja'
local hrkt_translit = wiktionary.require 'Module:Hrkt-translit'

local languages = { 'en', 'zh', 'es', 'fr', 'ar', 'ru', 'de', 'ja' }

local railway_operators = {
	Q23239 = 'amtrak',
	['Amtrak'] = 'amtrak',

	Q876720 = 'viarail',
	['VIA Rail Canada'] = 'viarail',
	['VIA Rail'] = 'viarail',
	['Via Rail Canada'] = 'viarail',
	['Via Rail'] = 'viarail',

	Q26334 = 'nationalrail',
	['National Rail'] = 'nationalrail',

	Q1937120 = 'translink',
	['Translink NI Railways'] = 'translink',

	Q73043 = 'ie',
	['Irish Rail'] = 'ie',
	['Iarnród Éireann'] = 'ie',

	Q122870674 = 'db',
	['DB InfraGO'] = 'db',
	['DB InfraGO AG'] = 'db',
	Q1152119 = 'db',
	['DB Station&Service'] = 'db',
	['DB Station&Service AG'] = 'db',
	Q896765 = 'db',
	['DB Netz'] = 'db',
	['DB Netz AG'] = 'db',
	['DB RegioNetz Infrastruktur GmbH'] = 'db',
	['Deutsche Bahn'] = 'db',
	['Deutsche Bahn AG'] = 'db',
	['DB'] = 'db',

	Q13646 = 'sncf',
	['SNCF'] = 'sncf',

	Q358752 = 'adif',
	['Adif'] = 'adif',
	['ADIF'] = 'adif',
	['Renfe'] = 'adif',
	Q83819 = 'adif',

	Q1060049 = 'rfi',
	['RFI'] = 'rfi',
	['RFI - Rete Ferroviaria Italiana'] = 'rfi',
	['Rete Ferroviaria Italiana'] = 'rfi',
	Q908916 = 'rfi',
	['Grandi Stazioni'] = 'rfi',

	Q1848025 = 'cd',
	['cz:SŽDC'] = 'cd',

	Q168082 = 'mav',
	['MÁV'] = 'mav',

	Q923337 = 'uz',
	['Укрзалізниця'] = 'uz',
	['УЗ'] = 'uz',

	Q660770 = 'rzd',
	['ОАО «РЖД»'] = 'rzd',
	['РЖД'] = 'rzd',

	Q525359 = 'cfl',
	['CFL'] = 'cfl',

	Q23076 = 'ns',
	['Nederlandse Spoorwegen'] = 'ns',
	['NS'] = 'ns',

	Q83835 = 'sbb',
	['SBB'] = 'sbb',
	['CFF'] = 'sbb',

	Q819425 = 'ir',
	['IR'] = 'ir',

	Q6867489 = 'pr',
	Q918096 = 'pr',
	['Pakistan Railways'] = 'pr',
	['Pakistan Railway'] = 'pr',
	['PR'] = 'pr',

	Q99633 = 'bvg',
	['Berliner Verkehrsbetriebe'] = 'bvg',
	['Berliner Verkehrsbetriebe (BVG)'] = 'bvg',

	Q1686811 = 'jnv',
	['JeNah'] = 'jnv',
	['JNV'] = 'jnv',
	['Jenaer Nahverkehrsgesellschaft mbH'] = 'jnv',

	Q1479375 = 'mtr',
	['香港鐵路有限公司 MTR Corporation Limited'] = 'mtr',
	['香港鐵路有限公司 MTR Corporation'] = 'mtr',
	Q14751 = 'mtr',
	['港鐵 MTR'] = 'mtr',
	['MTR'] = 'mtr',

	Q3271384 = 'rapidkl',
	['Rapid KL'] = 'rapidkl'
}

local own_ref_operators = { 'BVG', 'JNV' }

local station_class = {
	-- China (Q30949085)
	Q3183365 = 0, -- top class
	Q10864029 = 1,
	Q10880310 = 2,
	Q10866573 = 3,
	Q10925238 = 4,
	Q10882065 = 5,

	-- Taiwan (Q56148526)
	Q12820822 = 0, -- special
	Q10864031 = 1,
	Q10880311 = 2,
	Q10866574 = 3,
	Q15907831 = 4, -- easy
	Q11077284 = 5, -- calling class

	-- Germany (Q550637)
	Q18681579 = 1,
	Q18681660 = 2,
	Q18681688 = 3,
	Q18681690 = 4,
	Q18681691 = 5,
	Q18681692 = 6,
	Q18681693 = 7,
	-- AVG (Q129675144)
	Q129675088 = 1,
	Q129675089 = 2,

	-- Italy (Q56194580)
	Q56195103 = 1, -- platinum
	Q56195104 = 2, -- gold
	Q56195105 = 3, -- silver
	Q56195107 = 4, -- bronze

	-- United Kingdom (Q7888186)
	Q54883424 = 1, -- A
	Q54883641 = 2, -- B
	Q54883692 = 3, -- C
	Q54883781 = 3, -- C1
	Q54883829 = 3, -- C2
	Q54883881 = 4, -- D
	Q54883938 = 5, -- E
	Q54884015 = 6, -- F1
	Q54884061 = 7, -- F2

	-- Hungary (Q113027198)
	Q113027199 = 1, -- passenger
	Q113027200 = 2, -- passenger
	Q113027201 = 3, -- passenger
	Q113027202 = 4, -- passenger
	Q113027203 = 1, -- freight
	Q113027204 = 2, -- freight
	Q113027205 = 3 -- freight
}

local mtr_colors = {
	CEN = { color = '#aa0000', text = '#ffffff' },
	ADM = { color = '#3a86d4', text = '#ffffff' },
	TST = { color = '#ffef00', text = '#000000' },
	JOR = { color = '#69b72b', text = '#ffffff' },
	YMT = { color = '#cccccc', text = '#000000' },
	MOK = { color = '#be2700', text = '#ffffff' },
	PRE = { color = '#8674a1', text = '#ffffff' },
	SSP = { color = '#016258', text = '#ffffff' },
	CSW = { color = '#b5a265', text = '#000000' },
	LCK = { color = '#e04300', text = '#ffffff' },
	MEF = { color = '#1e90ff', text = '#ffffff' },
	LAK = { color = '#bb2200', text = '#ffffff' },
	KWF = { color = '#233d3a', text = '#ffffff' },
	KWH = { color = '#f1cc00', text = '#000000' },
	TWH = { color = '#a2b741', text = '#ffffff' },
	TSW = { color = '#bb2200', text = '#ffffff' },
	WHA = { color = '#aecff0', text = '#000000' },
	HOM = { color = '#a2cf5a', text = '#000000' },
	SKM = { color = '#669933', text = '#ffffff' },
	KOT = { color = '#007fff', text = '#ffffff' },
	LOF = { color = '#579e2f', text = '#000000' },
	WTS = { color = '#ffff00', text = '#000000' },
	DIH = { color = '#000000', text = '#ffffff' },
	CHH = { color = '#27408b', text = '#ffffff' },
	KOB = { color = '#c80815', text = '#ffffff' },
	NTK = { color = '#92b6a3', text = '#ffffff' },
	KWT = { color = '#ffffff', text = '#000000' },
	LAT = { color = '#0083be', text = '#000000' },
	YAT = { color = '#ffef00', text = '#000000' },
	TIK = { color = '#dcd144', text = '#000000' },
	KET = { color = '#95d0d0', text = '#000000' },
	HKU = { color = '#b8da89', text = '#000000' },
	SYP = { color = '#8b7ba0', text = '#000000' },
	SHW = { color = '#ffd280', text = '#6b4513' },
	WAC = { color = '#e1eb2b', text = '#000000' },
	CAB = { color = '#c8a2c8', text = '#000000' },
	TIH = { color = '#ff7d00', text = '#000000' },
	FOH = { color = '#4b8842', text = '#ffffff' },
	NOP = { color = '#e86220', text = '#000000' },
	QUB = { color = '#00918c', text = '#ffffff' },
	TAK = { color = '#bb2200', text = '#ffffff' },
	SWH = { color = '#ffcc00', text = '#000000' },
	SKW = { color = '#191970', text = '#ffffff' },
	HFC = { color = '#c01204', text = '#ffffff' },
	CHW = { color = '#38510e', text = '#ffffff' },
	TKO = { color = '#e60012', text = '#ffffff' },
	HAH = { color = '#2ea9df', text = '#000000' },
	POA = { color = '#f28500', text = '#000000' },
	LHP = { color = '#826f79', text = '#ffffff' },
	HOK = { color = '#fffafa', text = '#000000' },
	KOW = { color = '#aca28a', text = '#000000' },
	OLY = { color = '#4584c4', text = '#000000' },
	NAC = { color = '#f0ee86', text = '#000000' },
	TSY = { color = '#a1c6ca', text = '#000000' },
	SUN = { color = '#808080', text = '#c0c0c0' },
	TUC = { color = '#6a5acd', text = '#000000' },
	DIS = { color = '#005533', text = '#d4af37' },
	AIR = { color = '#808080', text = '#000000' },
	AWE = { color = '#ffffff', text = '#000000' },
	EXC = { color = '#94a8b0', text = '#ffffff' },
	HUH = { color = '#f08080', text = '#000000' },
	MKK = { color = '#006400', text = '#ffffff' },
	TAW = { color = '#05117e', text = '#ffffff' },
	SHT = { color = '#bb7796', text = '#ffffff' },
	FOT = { color = '#ffa500', text = '#ffffff' },
	RAC = { color = '#15ae69', text = '#ffffff' },
	UNI = { color = '#a2d7dd', text = '#ffffff' },
	TAP = { color = '#976e9a', text = '#ffffff' },
	TWO = { color = '#c89f05', text = '#ffffff' },
	FAN = { color = '#9acd32', text = '#ffffff' },
	SHS = { color = '#f6a600', text = '#ffffff' },
	LOW = { color = '#8dc476', text = '#ffffff' },
	LMC = { color = '#009e9b', text = '#ffffff' },
	WKS = { color = '#954535', text = '#ffffff' },
	MOS = { color = '#e0b0ff', text = '#000000' },
	HEO = { color = '#87cefa', text = '#000000' },
	TSH = { color = '#48d1cc', text = '#000000' },
	SHM = { color = '#fbec5d', text = '#000000' },
	CIO = { color = '#ffbf00', text = '#000000' },
	STW = { color = '#ffc0cb', text = '#000000' },
	CKT = { color = '#ffd280', text = '#000000' },
	HIK = { color = '#8fbe6c', text = '#182f4f' },
	KAT = { color = '#ff8c00', text = '#000000' },
	SUW = { color = '#d08a00', text = '#000000' },
	TKW = { color = '#a9e2f3', text = '#000000' },
	ETS = { color = '#ffff00', text = '#000000' },
	AUS = { color = '#b45529', text = '#ffffff' },
	TWW = { color = '#a81c07', text = '#ffffff' },
	KSR = { color = '#cc5500', text = '#ffffff' },
	YUL = { color = '#40f5f5', text = '#000000' },
	LOP = { color = '#ffb3bf', text = '#000000' },
	TIS = { color = '#fc8a17', text = '#000000' },
	SIH = { color = '#7fffd4', text = '#000000' },
	TUM = { color = '#035f94', text = '#ffffff' },
	OCP = { color = '#00bfff', text = '#ffffff' },
	WCH = { color = '#ffff00', text = '#000000' },
	LET = { color = '#ff7f00', text = '#ffffff' },
	SOH = { color = '#74b11b', text = '#ffffff' },
	TCT = { color = '#274060', text = '#ffffff' },
	NPT = { color = '#274060', text = '#ffffff' },
	WEK = { color = '#808080', text = '#000000' },
}

---@class Set<T>: table<t, true>

---@generic T
---@param table T[]
---@return Set<T>
local function set(table)
	local res = {}
	for _, val in pairs(table) do
		res[val] = true
	end
	return res
end

local function csv(name, header, sep)
	sep = sep or ','
	local lines = io.lines(name)
	if header then
		lines()
	end
	return function()
		local line = lines()
		if line == nil then
			return nil
		else
			return string.gmatch(line .. sep, '(.-)\r?' .. sep)
		end
	end
end

local function wikidata_item(col)
	local id = col:sub(32)
	if id:sub(1, 1) == 'Q' then
		return id
	elseif col == '' or col:sub(1, 36) == 'http://www.wikidata.org/.well-known/' then
		return nil
	else
		print(string.format('unknown item value %s', col))
		return nil
	end
end

local track_data = {}
for col in csv('track_data.csv', true) do
	local item = col():sub(32)
	local electrification = wikidata_item(col())
	local uses = wikidata_item(col())
	local voltage = tonumber(col())
	local frequency = tonumber(col()) or 0

	local electrified = ''
	if electrification == nil or electrification == 'Q11663772' then
		electrified = 'no'
	elseif uses == 'Q110701' then
		electrified = 'contact_line'
	elseif uses == 'Q748825' then
		electrified = 'rail'
	elseif uses == 'Q109180474' then
		electrified = '4th_rail'
	elseif uses == 'Q2312102' then
		electrified = 'ground-level_power_supply'
	elseif uses == 'Q267298' then
		goto continue
	else
		electrified = 'yes'
		if uses ~= nil then
			print(string.format('unknown use %s for %s', uses, item))
		end
	end

	track_data[item] = {
		electrified = electrified,
		voltage     = voltage,
		frequency   = frequency,
	}
	::continue::
end

local station_refs = {}
local db_codes = {}
local uic_ids = {}
local station_refs_count = 0
for col in csv('station_refs.csv', true) do
	local item = col():sub(32)
	local ref = station_refs[item] or { wikidata = item }

	for _ = 1, 3 do
		if ref.operator == nil then
			ref.operator = ref.operator or railway_operators[col():sub(32)]
		else
			col()
		end
	end

	local class_item = col():sub(32)
	ref.class = ref.class or station_class[class_item]
	if class_item ~= '' and ref.class == nil and class_item ~= 'Q55678' then
		print(string.format('Unknown station class %s for %s', class_item, item))
	end

	local fields = {
		'uic_code',
		'ibnr_id',
		'uk_code',
		'db_code',
		'db_ref',
		'db_ref',
		'db_ref',
		'sncf_code',
		'sncf_ref',
		'rfi_ref',
		'adif_code',
		'ir_code',
		'pr_code',
		'cr_code',
		'mtr_code',
		'jr_ref',
	}
	for _, field in ipairs(fields) do
		if ref[field] == nil or ref[field] == '' then
			ref[field] = col()
		else
			col()
		end
	end

	if ref.uic_code == '' and ref.adif_code ~= '' then
		ref.uic_code = '71' .. ref.adif_code
	end

	if ref.jr_ref ~= '' then
		local script = ja.script(ref.jr_ref)
		if script ~= 'Kana' and script ~= 'Hira+Kana' then
			--print(string.format('Unknown script %s for %s jr_ref (%s)', script, item, ref.jr_ref))
		else
			ref.jr_ref_latn = hrkt_translit.tr(ref.jr_ref, 'ja')
		end
	end

	station_refs[item] = ref
	if ref.uic_code ~= '' then
		uic_ids[ref.uic_code] = ref
	end
	if ref.ibnr_id ~= '' then
		uic_ids[ref.ibnr_id] = ref
	end
	if ref.db_code ~= '' then
		db_codes[ref.db_code] = ref
	end

	station_refs_count = station_refs_count + 1
end

local skip_adif = set {
	'11602', '77310', '79316',                                                                                                                            -- France
	'22402', '51045', '51102', '51128', '51185', '51243', '52001', '55129', '55293', '56002', '56101', '56267', '57117', '57174', '57307', '57315', '57497', -- Portugal
}
local unknown_adif_count = 0
for col in csv('adif_station_refs.csv', false) do
	local uic = col()
	local code = col()

	if uic:match('^87') or -- France
		uic:match('^94') or -- Portugal
		skip_adif[uic]
	then
		goto continue
	end

	local ref = uic_ids['71' .. uic]
	if ref == nil then
		local name = col()
		local lat = col()
		local lon = col()

		local msg = string.format('Unknown adif station \'%s\' (https://www.adif.es/w/%s-%s#x)', name, uic, code)
		if lat ~= '0' or lon ~= '0' then
			msg = string.format('%s at https://www.openstreetmap.org/#map=19/%s/%s', msg, lat, lon)
		end
		print(msg)
		unknown_adif_count = unknown_adif_count + 1
	else
		ref.adif_ref = uic .. '-' .. code
	end

	::continue::
end
print(string.format('%d unknown adif stations', unknown_adif_count))

print(string.format('Loaded station %d refs', station_refs_count))

---@type { name: string, subname?: string, key: string, seperator?: string, values: Set<string>, skip_value?: boolean, routes?: integer[], lat?: { [1]: number, [2]: number }, lon?: { [1]: number, [2]: number } }[]
local railway_signaling = {
	{
		name = 'ETCS',
		values = set { '1', '2', '3' },
	},

	{
		name = 'CBTC',
		values = set { 'uto', 'sto', 'dto' }
	},

	-- China
	{
		name = 'CTCS',
		values = set { '0', '1', '2', '3D', '3', '4' },
	},

	-- Germany
	{ name = 'ZBS' },
	{ name = 'GNT' },
	{ name = 'ZUB 262', key = 'gnt', values = set { 'zub262' }, skip_value = true },
	{ name = 'ZUB 122', key = 'gnt', values = set { 'zub122' }, skip_value = true },
	{ name = 'LZB' },
	{ name = 'PZB' },

	-- France / South Korea
	{
		name = 'TVM',
		seperator = '-',
		values = set { '300', '430' },
	},
	{ name = 'KVB' },

	-- Italy
	{ name = 'SCMT' },
	{ name = 'SSC' },

	-- Poland
	{ name = 'SHP' },

	-- Czechia
	{ name = 'LS' },

	-- Hungary
	{ name = 'EVM' },

	-- Belgium
	{ name = 'TBL',      values = set { '1', '1+', '2' } },

	-- Netherlands
	{ name = 'ATB-NG' },
	{ name = 'ATB-EG' },
	{ name = 'ATB' },
	{ name = 'ZUB 222c', key = 'zub222c' },

	-- Ireland
	{ name = 'CAWS' },

	-- Spain
	{ name = 'ASFA 200', key = 'asfa_200' },
	{ name = 'ASFA' },

	{ name = 'EBICab',   values = set { '700', '900' } },

	-- Sweden / Norway
	{
		name = 'ATC',
		subname = 's',
		lat = { 50, 80 },
		lon = { 0, 30 },
	},

	-- Finland
	{ name = 'JKV' },

	-- Denmark
	{ name = 'ZUB 123', key = 'zub123' },

	-- Switzerland
	{ name = 'ZSI 127', key = 'zsi127' },

	-- England
	{ name = 'TPWS' },
	{ name = 'AWS' },

	-- United States
	{ name = 'ETMS' },
	{ name = 'ITCS' },
	{ name = 'ACSES' },
	{ name = 'PTC' },

	-- Japan
	{
		name = 'ATACS',
		routes = {
			1862832, 11366305, -- Senseki Line
			1956420,   -- Saikyo Line
			10077097, 10089179, -- Koumi Line
			1872548, 8506268, -- Joban Line
		},
	},
	{
		name = 'ATC-NS',
		routes = {
			5263977, -- Tokaido Shinkansen
			1837932, -- Sanyo Shinkansen
			4500371, 4500369, -- Taiwan High Speed Rail
		},
	},
	{
		name = 'DS-ATC',
		routes = {
			1860622, -- Tohoku Shinkansen
			5947648, -- Hokkaido Shinkansen
			1860557, -- Joetsu Shinkansen
			1926393, -- Hokuriku Shinkansen
		},
	},
	{
		name = 'KS-ATC',
		routes = {
			1565609, -- Kyushu Shinkansen
			7356208, -- Nishi Kyushu Shinkansen
		},
	},
	{
		name = 'D-ATC',
		routes = {
			5376382,                     -- Yamanote Line
			5195691, 5195692, 10358983, 10358982, -- Keihin-Tōhoku Line
		},
	},
	{
		name = 'New CS-ATC',
		routes = {
			-- Tokyo Metro
			8026155, 443272,                       -- Hibiya Line
			443281, 8026074,                       -- Ginza Line
			8015932, 8015930, 443282, 8015931,     -- Marunouchi Line
			5371620, 8026141, 9812332, 9812331, 10840434, -- Tōzai Line
			7730199, 8026070,                      -- Namboku Line
			443269, 11371520,                      -- Yūrakuchō Line
			8026050, 8026053, 443284, 2790323,     -- Chiyoda Line
			443279, 8026159,                       -- Hanzomon Line
			5375678, 8026168, 12250433, 12250868, 12250872, -- Fukutoshin Line

			9341815, 404834,                       -- Tokyu-Denentoshi Line
			9288982, 1947536,                      -- Tokyu-Toyoko Line
			2549404, 4589046,                      -- Tsukuba Express Line
		},
	},
	{
		name = 'CS-ATC',
		routes = {
			8015932, 8015930, 443282, 8015931, -- Marunouchi Line

			-- Nagoya Municipal Subway
			421984, 8030795, -- Higashiyama Line
			1905460, 8031616, -- Meijō Line
			7712570, 8030805, -- Meikō Line
			1920724, 8030807, -- Tsurumai Line
			6566407, 8030793, -- Sakura Dōri Line
			7712571, 6566412, -- Kamiiida Line

			-- Osaka Metro
			444891, 8028803, -- Sennichimae Line
			444878, 8028831, -- Nagahori Tsurumi-Ryokuchi Line
			444889, 8028827, -- Imazatosuji Line
		},
	},
	{
		name = 'WS-ATC',
		routes = {
			-- Osaka Metro
			2411153, 8028808, -- Midōsuji Line
			444907, 8028880, -- Tanimachi Line
			444899, 8028854, -- Yotsubashi Line
			444881, 8028818, -- Chūō Line
			1864803, 10258968, -- Kintetsu Keihanna-Linie
			444905, 8028830, -- Sakaisuji Line
		},
	},
}

local railway_signaling_routes = {}
for _, data in pairs(railway_signaling) do
	if data.key == nil then
		data.key = data.name:lower()
	end
	data.key = 'railway:' .. data.key

	if data.routes ~= nil then
		for _, route in pairs(data.routes) do
			railway_signaling_routes[tostring(route)] = data
			railway_signaling_routes[route] = data
		end
	end
end

---@param tag string
---@return string, string
local function lifecycle(tag)
	local val = Find(tag)
	if val == 'construction' or val == 'proposed' or val == 'disused' then
		local res = Find(val .. ':' .. tag)
		if res == '' then
			res = Find(val)
		end
		return res, val
	end

	return val, ''
end

---@param key string
---@param lc string
---@return string
local function find_lifecycle(key, lc)
	local val = ''
	if lc ~= '' then
		val = Find(lc .. ':' .. key)
	end
	if val == '' then
		val = Find(key)
	end
	return val
end

local function numAttr(name, key, lc)
	local val = tonumber(find_lifecycle(key or name, lc))
	if val == nil then
		AttributeNumeric(name, -1)
	else
		AttributeNumeric(name, val)
	end
end

---@param prefix string
---@param native string
---@param native_lang? string
local function name_attrs(prefix, native, native_lang)
	if native == '' then
		native = Find(prefix .. 'name')
	end
	Attribute('name', native)

	local translation = false
	for _, lang in ipairs(languages) do
		local name = 'name:' .. lang
		local val = Find(prefix .. name)
		if val ~= '' then
			translation = true
			Attribute(name, val)
		end
	end

	if not translation and native_lang ~= nil then
		local tr, fail = wlangs.getByCode(native_lang):transliterate(native)
		if not fail then
			Attribute('name-Latn', tr)
		end
	end
end

local railway_usage = {
	station         = 'Passenger',
	halt            = 'Passenger',
	tram_stop       = 'Passenger',

	yard            = 'Freight',

	site            = 'Operational',
	service_station = 'Operational',
	junction        = 'Operational',
	crossover       = 'Operational',
}

---@param node boolean
---@param railway string
---@param lc string
---@return boolean
local function railway_node_or_way(node, railway, lc)
	local usage = railway_usage[railway]
	if usage == nil then
		return false
	end
	if lc ~= '' then
		return true
	end

	local operator_id = nil
	local operators =
		Find('operator') .. ';' ..
		Find('operator:wikidata') .. ';' ..
		Find('network') .. ';' ..
		Find('network:wikidata')
	for operator in string.gmatch(operators, '[^;]+') do
		local id = railway_operators[operator]
		if id then
			operator_id = id
			break
		end
	end

	local ref = Find('railway:ref')
	if ref == '' then
		ref = Find('ref:crs')
	end

	local name = Find('name')
	local lang = nil

	local class = tonumber(Find('railway:station_category'))

	local ext_ref = station_refs[Find('wikidata')]
	if ext_ref == nil and operator_id == 'db' then
		ext_ref = db_codes[ref]
	end
	if ext_ref == nil then
		ext_ref = uic_ids[Find('uic_ref')]
	end

	if ext_ref ~= nil and operator_id == nil then
		operator_id = ext_ref.operator
	end
	if ref == '' and (operator_id == 'ir' or operator_id == 'pr' or operator_id == 'mtr' or operator_id == 'rapidkl') then
		ref = Find('ref'):gsub(';', ' ')
	end

	local web_ref = nil
	local ref_latn = nil
	if ext_ref ~= nil then
		if class == nil then
			class = ext_ref.class
		end

		if operator_id == 'nationalrail' then
			if ref == '' then
				ref = ext_ref.uk_code
			end
		elseif operator_id == 'db' then
			web_ref = ext_ref.db_ref
			if web_ref == '' then
				web_ref = Find('ref:station')
			end
			if ref == '' then
				ref = ext_ref.db_code
			end
		elseif operator_id == 'sncf' then
			web_ref = ext_ref.sncf_ref
			if ref == '' and ext_ref.sncf_code:sub(1, 2) == 'FR' then
				ref = ext_ref.sncf_code:sub(3)
			end
		elseif operator_id == 'rfi' then
			web_ref = ext_ref.rfi_ref
		elseif operator_id == 'adif' then
			web_ref = ext_ref.adif_ref
		elseif operator_id == 'cd' or operator_id == 'mav' then
			web_ref = ext_ref.uic_code
		elseif operator_id == 'uz' then
			web_ref = ext_ref.uic_code:sub(-3)
		elseif operator_id == 'sbb' then
			web_ref = ext_ref.uic_code:match('^850*(.+)$')
		elseif operator_id == 'ir' then
			if ref == '' then
				ref = ext_ref.ir_code
			end
		elseif operator_id == 'pr' then
			if ref == '' then
				ref = ext_ref.pr_code
			end
		elseif operator_id == 'mtr' then
			if ref == '' then
				ref = ext_ref.mtr_code
			end
		end

		if ref == '' then
			ref = ext_ref.cr_code
		end
	end

	if operator_id == 'uz' then
		lang = 'uk'
	elseif operator_id == 'rzd' then
		lang = 'ru'
	elseif operator_id == 'rapidkl' then
		local name_ms = Find('name:ms')
		if name == ref .. ' ' .. name_ms then
			name = name_ms
		elseif ref ~= '' and name:sub(1, #ref + 1) == ref .. ' ' then
			name = name:sub(#ref + 2)
		elseif ref == '' and name_ms ~= '' and name:sub(- #name_ms - 1) == ' ' .. name_ms then
			ref = name:sub(1, - #name_ms - 2)
			name = name:sub(- #name_ms)
		else
			print(string.format("Unexpected rapidkl station name: '%s' instead of '%s %s'", name, ref, name_ms))
		end
	end

	for _, own_ref in ipairs(own_ref_operators) do
		if ref ~= '' then
			break
		end

		local operator = own_ref:lower()
		if operator_id == nil or operator_id == operator then
			ref = Find('ref:' .. own_ref)
			if ref ~= '' then
				operator_id = operator
			end
		end
	end

	if ref == '' then
		local centroid = Centroid()
		if centroid ~= nil then
			local lat = centroid[1]
			local lon = centroid[2]
			local use_ref = lon > 90 or -- Asia
				(lat > 55 and lon > 4 and lon < 24) -- Norway and Sweden
			if use_ref then
				ref = Find('ref'):gsub(';', ' ')
			end
		end
	end
	if ref == '' and ext_ref ~= nil then
		ref = ext_ref.jr_ref
		ref_latn = ext_ref.jr_ref_latn
	end

	if ref == '' and name == '' then
		return true
	end

	if node then
		Layer('station')
	else
		LayerAsCentroid('station')
	end

	if operator_id ~= nil then
		Attribute('operator', operator_id)
	end

	if operator_id == 'mtr' then
		local color = mtr_colors[ref]
		if color ~= nil then
			Attribute('color', color.color)
			Attribute('text', color.text)
		end
	end

	name_attrs('', name, lang)
	Attribute('ref', ref)
	if ref_latn ~= nil then
		Attribute('ref-Latn', ref_latn)
	end
	if web_ref ~= nil and web_ref ~= '' then
		Attribute('web_ref', web_ref)
	end

	Attribute('usage', usage)
	AttributeNumeric('z', class or 10)

	local station = Find('station')
	if railway == 'tram_stop' or station == 'tram' or station == 'light_rail' then
		MinZoom(14)
	elseif station == 'subway' then
		MinZoom(13)
	end

	return true
end

node_keys = {
	'place=country',
	'place=city',
	'place=town',
	'place=village',
	'railway',
}
function node_function()
	local place = Find('place')
	if place == 'country' or place == 'city' or place == 'town' or place == 'village' then
		Layer('place')

		name_attrs('', '')

		if place == 'country' then
			Attribute('type', place)
			AttributeNumeric('z', 2)
		else
			if Find('capital') == 'yes' then
				Attribute('type', 'capital')
			else
				Attribute('type', place)
			end

			local zoom
			local population = tonumber(Find('population'))
			if population == nil then
				if place == 'village' then
					population = 5e3
				elseif place == 'town' then
					population = 50e3
				else
					population = 100e3
				end
			end

			if population <= 1e3 then
				zoom = 13
			elseif population <= 5e3 then
				zoom = 12
			elseif population <= 10e3 then
				zoom = 11
			elseif population <= 25e3 then
				zoom = 10
			elseif population <= 50e3 then
				zoom = 9
			elseif population <= 100e3 then
				zoom = 8
			elseif population <= 250e3 then
				zoom = 7
			elseif population <= 500e3 then
				zoom = 6
			elseif population <= 1e6 then
				zoom = 5
			elseif population <= 5e6 then
				zoom = 4
			else
				zoom = 3
			end

			MinZoom(zoom)
			AttributeNumeric('z', zoom)
		end
	end

	local railway = Find('railway')
	if railway == 'subway_entrance' then
		Layer('entrance')

		Attribute('ref', Find('ref'))
	else
		railway_node_or_way(true, railway, '')
	end
end

---@param value string
---@return boolean
local function no_or_missing(value)
	return value == '' or value == 'no'
end

local function has_with_sides(prefix)
	return
		not no_or_missing(Find(prefix)) or
		not no_or_missing(Find(prefix .. ':left')) or
		not no_or_missing(Find(prefix .. ':right')) or
		not no_or_missing(Find(prefix .. ':side'))
end

local isRailway = set { 'rail', 'narrow_gauge', 'light_rail', 'tram', 'subway', 'monorail', 'funicular' }

way_keys = {
	'boundary=administrative',
	'natural=water',
	'railway',
	'building=train_station',
}
function way_function()
	if Find('boundary') == 'administrative' and Find('maritime') ~= 'yes' then
		local admin_level = tonumber(Find('admin_level'))
		if admin_level == 2 or admin_level == 4 then
			Layer('boundary', false)
			AttributeNumeric('level', admin_level)
			if admin_level == 4 then
				MinZoom(5)
			end
		end
	end

	if Find('natural') == 'water' and Find('water') ~= 'wastewater' then
		local area = Area() / 1e6
		if area > 0.001 then
			Layer('water', true)
			if area < 0.01 then
				MinZoom(12)
			elseif area < 0.1 then
				MinZoom(10)
			elseif area < 0.5 then
				MinZoom(8)
			elseif area < 1 then
				MinZoom(7)
			elseif area < 5 then
				MinZoom(5)
			end
		end
	end

	if Find('building') == 'train_station' then
		Layer('station_area', true)
	end

	local railway, lc = lifecycle('railway')

	if railway_node_or_way(false, railway, lc) then
		return
	end

	if railway == 'platform' then
		if lc == '' then
			local area = Find('type') == 'multipolygon' or Find('area') == 'yes'
			Layer('platform', area)
			AttributeBoolean('area', area)

			local ref = Find('local_ref')
			if ref == '' then
				ref = Find('ref')
			end
			if not ref:find(';') then
				Attribute('ref', ref)
			end
		end
		return
	end

	local is_railway = isRailway[railway] or (railway == '' and lc ~= '')
	if is_railway and no_or_missing(find_lifecycle('building', lc)) then
		Layer('railway', false)
		Attribute('lifecycle', lc)

		Attribute('ref', Find('railway:track_ref'), 14)

		local usage = find_lifecycle('usage', lc)
		if find_lifecycle('highspeed', lc) == 'yes' then
			Attribute('class', 'highspeed')
		elseif Find('service') ~= '' then
			Attribute('class', 'service')
			MinZoom(10)
		else
			Attribute('class', usage)
			if usage == 'main' then
			elseif usage == 'branch' then
				MinZoom(6)
			else
				MinZoom(10)
			end
		end

		if not no_or_missing(Find('tunnel')) then
			Attribute('structure', 'Tunnel')
		elseif not no_or_missing(Find('bridge')) then
			Attribute('structure', 'Bridge')
		elseif has_with_sides('embankment') then
			Attribute('structure', 'Embankment')
		elseif has_with_sides('cutting') then
			Attribute('structure', 'Cutting')
		else
			Attribute('structure', 'Ground')
		end

		local tunnel_name = Find('tunnel:name')
		if tunnel_name ~= '' then
			name_attrs('tunnel:', tunnel_name)
		else
			local bridge_name = Find('bridge:name')
			if bridge_name ~= '' then
				name_attrs('bridge:', bridge_name)
			end
		end

		local mode = Find('railway:traffic_mode')
		if usage == 'industrial' then
			usage = 'Industrial'
		elseif usage == 'tourism' then
			usage = 'Tourism'
		elseif usage == 'test' then
			usage = 'Testing'
		elseif mode == 'mixed' then
			usage = 'Mixed'
		elseif mode == 'freight' then
			usage = 'Freight'
		elseif mode == 'passenger' or (railway == 'light_rail' or railway == 'tram' or railway == 'subway' or railway == 'monorail') then
			usage = 'Passenger'
		else
			usage = ''
		end
		Attribute('usage', usage)

		local color = Find('colour')
		if color == '' then
			for _ in NextRelation do
				local col = FindInRelation('colour')
				if col ~= '' then
					color = col
					if FindInRelation('by_night') ~= 'only' then
						break
					end
				end
			end
		end
		if color ~= '' then
			Attribute('color', color)
		end

		numAttr('gauge', nil, lc)

		local maxspeed = find_lifecycle('maxspeed', lc)
		local mph = maxspeed:sub(-4) == ' mph'
		if mph then
			maxspeed = maxspeed:sub(1, -4)
		end
		local n = tonumber(maxspeed)
		if n == nil then
			AttributeNumeric('maxspeed', -1)
		else
			AttributeNumeric('maxspeed', n)
			AttributeBoolean('maxspeed_mph', mph)
		end

		local electrified = find_lifecycle('electrified', lc)

		local voltage
		local frequency
		if electrified ~= '' and electrified ~= 'no' then
			voltage = tonumber(find_lifecycle('voltage', lc))
			frequency = tonumber(find_lifecycle('frequency', lc))
		end

		if electrified == '' or not voltage or not frequency then
			local track = track_data[Find('wikidata')]
			if track ~= nil then
				if electrified == '' then
					electrified = track.electrified
				end
				voltage = voltage or track.voltage
				frequency = frequency or track.frequency
			end
		end
		if electrified == '' or not voltage or not frequency then
			RestartRelations()
			for _ in NextRelation do
				local track = track_data[FindInRelation('wikidata')]
				if track ~= nil then
					if electrified == '' then
						electrified = track.electrified
					end
					voltage = voltage or track.voltage
					frequency = frequency or track.frequency
				end
			end
		end

		if electrified == 'no' then
			voltage = -2
			frequency = -2
		end

		Attribute('electrification', electrified)
		AttributeNumeric('voltage', voltage or -1)
		AttributeNumeric('frequency', frequency or -1)

		local signalingNo = false
		for _, data in pairs(railway_signaling) do
			if data.lat ~= nil then
				local centroid = Centroid()
				if centroid ~= nil then
					if data.lat[1] > centroid[1] or centroid[1] > data.lat[2] or
						data.lon[1] > centroid[2] or centroid[2] > data.lon[2] then
						goto continue
					end
				end
			end

			local val = find_lifecycle(data.key, lc)
			if val == '' then
			elseif val == 'no' then
				signalingNo = true
			elseif val == 'yes' then
				Attribute('signaling', data.name)
				if data.subname ~= nil then
					Attribute('signaling_sub', data.subname)
				end
				return
			elseif data.values ~= nil and data.values[val] then
				Attribute('signaling', data.name)
				if data.skip_value ~= true then
					Attribute('signaling_version', (data.seperator or ' ') .. val:upper())
				end
				if data.subname ~= nil then
					Attribute('signaling_sub', data.subname)
				end
				return
			end

			::continue::
		end

		RestartRelations()
		for rel in NextRelation do
			local data = railway_signaling_routes[rel]
			if data ~= nil then
				Attribute('signaling', data.name)
				return
			end
		end

		if signalingNo then
			Attribute('signaling', 'no')
		else
			Attribute('signaling', '')
		end
	end
end

function relation_scan_function()
	local id = Id()
	if railway_signaling_routes[id] or bake.station_areas[id] then
		Accept()
		return
	end

	local type = Find('type')
	if type == 'route' and Find('colour') ~= '' and isRailway[Find('route')] then
		Accept()
	end
end

function relation_function()
	if bake.station_areas[Id()] then
		Layer('station_area', true)
	end
end

function attribute_function(attr, layer)
	if layer == 'ocean' then
		return { class = 'ocean' }
	else
		return attr
	end
end

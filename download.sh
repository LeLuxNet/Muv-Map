set -e

if [ ! -d "water" ]; then
	echo "Downloading water"
	curl -f 'https://osmdata.openstreetmap.de/download/water-polygons-split-4326.zip' -o water.zip
	unzip water.zip
	mv water-polygons-split-4326 water
	rm water.zip
	echo
fi

cd wiktionary
if [ ! -d "Module" ]; then
	echo "Download wiktionary modules"
	curl -f https://kaikki.org/dictionary/wiktionary-modules.tar.gz | tar zxf -
fi
cd ..

if [ ! -f "adif_station_refs.csv" ]; then
	MAX_PAGES=1
	for (( i = 1; i <= $MAX_PAGES; i++ )); do
		echo "Updating adif station refs (page $i/$MAX_PAGES)"
		RESPONSE=$(curl -f --compressed -s -X POST "https://www.adif.es/informacion-al-usuario/estaciones?p_p_id=es_adif_portlet_ajaxsearch_result_AjaxSearchResultsPortlet&p_p_lifecycle=2&p_p_resource_id=%2Fdefault%2Fget-data" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36" --data-raw "_es_adif_portlet_ajaxsearch_result_AjaxSearchResultsPortlet_currentPage=$i")
		MAX_PAGES=$(echo "$RESPONSE" | jq '.payload.totalPages')
		echo "$RESPONSE" | jq '.payload.content[] | (.link | match("https:\\/\\/www\\.adif\\.es\\/w\\/(\\d{5})-(.+)\\?.+") | .captures | map(.string)) + [.title, .localizacion.latitude, .localizacion.longitude] | join(",")' -r >> adif_station_refs.csv
	done
fi

echo "Updating tracks"
curl -f --compressed -H "Accept: text/csv" "https://query.wikidata.org/sparql" --data-urlencode query="$(cat track_query.sparql)" > track_data.csv

echo "Updating stations refs"
curl -f --compressed -H "Accept: text/csv" "https://query.wikidata.org/sparql" --data-urlencode query="$(cat station_query.sparql)" > station_refs.csv

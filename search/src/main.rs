use std::{
    collections::HashMap,
    env::args,
    error::Error,
    fs::File,
    io::{BufReader, Seek, SeekFrom},
    process::exit,
    result,
    sync::Mutex,
};

use muv_osm::format::pbf::{self, Data, DataEntry, DataIter, DenseNodes, Reader, StringTable, Way};
use serde::Serialize;

type Result<T> = result::Result<T, Box<dyn Error>>;

#[derive(Serialize)]
struct Collection {
    name: String,
    fields: Vec<Field>,
}

#[derive(Serialize)]
struct Field {
    name: &'static str,
    r#type: &'static str,
    locale: Option<&'static str>,
    optional: bool,
}

#[derive(Serialize)]
struct Station {
    id: String,
    #[serde(flatten)]
    station: PartialStation,
    location: (f64, f64),
}

#[derive(Clone, Serialize)]
struct PartialStation {
    name: String,
    name_en: Option<String>,
}

#[derive(Default)]
struct StringTableMapper<'a> {
    m: HashMap<&'a str, (bool, &'a mut Option<u32>)>,
}

impl<'a> StringTableMapper<'a> {
    fn add(mut self, s: &'a str, val: &'a mut Option<u32>) -> Self {
        self.m.insert(s, (false, val));
        self
    }

    fn add_required(mut self, s: &'a str, val: &'a mut Option<u32>) -> Self {
        self.m.insert(s, (true, val));
        self
    }

    fn run(mut self, st: StringTable<'_>) -> result::Result<bool, pbf::Error> {
        for (i, s) in (0..).zip(st) {
            let Some(entry) = self.m.get_mut(s?) else {
                continue;
            };
            assert!(entry.1.replace(i).is_none());
        }

        for (required, val) in self.m.into_values() {
            if required && val.is_none() {
                return Ok(false);
            }
        }

        Ok(true)
    }
}

struct BlockDecoder<'a> {
    name_st: Option<u32>,
    name_en_st: Option<u32>,
    railway_st: Option<u32>,
    station_st: Option<u32>,
    halt_st: Option<u32>,

    data: Data<'a>,
}

impl BlockDecoder<'_> {
    fn node(
        &self,
        keys_vals: impl IntoIterator<Item = pbf::Result<(u32, u32)>>,
    ) -> result::Result<Option<PartialStation>, pbf::Error> {
        let mut name_val = None;
        let mut name_en_val = None;
        let mut found = false;
        for key_val in keys_vals {
            let (key, val) = key_val?;
            let key = Some(key);
            let val = Some(val);

            if key == self.name_st {
                name_val = val;
            } else if key == self.name_en_st {
                name_en_val = val;
            } else if key == self.railway_st && (val == self.station_st || val == self.halt_st) {
                found = true;
            }
        }
        if !found {
            return Ok(None);
        }
        let Some(name_val) = name_val else {
            return Ok(None);
        };

        let mut name = None;
        let mut name_en = None;
        let max_si = name_val.max(name_en_val.unwrap_or_default());
        for (i, s) in (0..)
            .zip(self.data.string_table.clone())
            .take(usize::try_from(max_si).unwrap() + 1)
        {
            let s = s?;
            if i == name_val {
                name = Some(s.to_owned());
            } else if Some(i) == name_en_val {
                name_en = Some(s.to_owned());
            }
        }

        Ok(Some(PartialStation {
            name: name.unwrap(),
            name_en,
        }))
    }
}

fn main() -> Result<()> {
    let Some(key) = args().nth(1) else {
        eprintln!("usage: <typesense api key>");
        exit(1);
    };
    let url = "https://typesense.lelux.net/api";
    let collection_name = "muv-stations";

    let mut f = File::open("/home/ginnythecat/regions/japan.osm.pbf")?;

    let collection = Collection {
        name: collection_name.into(),
        fields: vec![
            Field {
                name: "name",
                r#type: "string",
                locale: None,
                optional: false,
            },
            Field {
                name: "name_en",
                r#type: "string",
                locale: Some("en"),
                optional: true,
            },
            Field {
                name: "location",
                r#type: "geopoint",
                locale: None,
                optional: false,
            },
        ],
    };
    println!(
        "update schema: {:?}",
        ureq::post(&format!("{url}/collections"))
            .set("X-Typesense-Api-Key", &key)
            .send_json(collection)
    );

    let stations: Mutex<Vec<_>> = Mutex::default();
    let way_stations: Mutex<HashMap<u64, (u64, PartialStation)>> = Mutex::default();

    /* let s = Scribuntor::new(|m| {
        let path = format!("../wiktionary/Module/{m}.txt");
        Some(read(path).unwrap())
    });
    let langs = wiktionary::Languages::new(&s);
    let langs_ru = langs.by_code("ru"); */

    let r = Reader::new(BufReader::new(&mut f))?;
    r.par_for_each(|i, blob| {
        println!("node block {i}");

        let blob = blob.decompress()?;

        let data = Data::decode(&blob)?;

        let mut railway_st = None;
        let mut station_st = None;
        let mut halt_st = None;
        let mut name_st = None;
        let mut name_en_st = None;

        let complete = StringTableMapper::default()
            .add_required("railway", &mut railway_st)
            .add("station", &mut station_st)
            .add("halt", &mut halt_st)
            .add_required("name", &mut name_st)
            .add("name:en", &mut name_en_st)
            .run(data.string_table.clone())?;

        if !complete || (station_st.is_none() && halt_st.is_none()) {
            println!("skip");
            return Ok(());
        }

        let dec = BlockDecoder {
            name_st,
            name_en_st,
            railway_st,
            station_st,
            halt_st,

            data,
        };

        for entry in DataIter::new(&blob) {
            match entry? {
                DataEntry::DenseNodes(d) => {
                    let mut dns = DenseNodes::decode(d)?;
                    while let Some(dn) = dns.next()? {
                        let id = dn.id;
                        let lat = dn.lat;
                        let lon = dn.lon;

                        let Some(station) = dec.node(dn)? else {
                            continue;
                        };

                        let station = Station {
                            id: format!("n{}", id),
                            station,
                            location: (dec.data.lat(lat), dec.data.lon(lon)),
                        };
                        stations.lock().unwrap().push(station);
                    }
                }
                DataEntry::Node(_) => todo!(),
                DataEntry::Way(w) => {
                    let mut way = Way::decode(w)?;

                    let Some(station) = dec.node(way.keys_vals)? else {
                        continue;
                    };

                    let node = way.refs.next().unwrap()?;

                    way_stations.lock().unwrap().insert(node, (way.id, station));
                }
                DataEntry::Relation(_) | DataEntry::ChangeSet(_) => {}
            }
        }

        Ok(())
    })
    .unwrap();

    let way_stations = way_stations.into_inner().unwrap();

    f.seek(SeekFrom::Start(0))?;
    let r = Reader::new(BufReader::new(&mut f))?;
    r.par_for_each(|i, blob| {
        println!("way block {i}");

        let blob = blob.decompress()?;

        let data = Data::decode(&blob)?;

        for entry in DataIter::new(&blob) {
            match entry? {
                DataEntry::DenseNodes(dn) => {
                    let mut dns = DenseNodes::decode(dn)?;
                    while let Some(dn) = dns.next()? {
                        let Some((id, station)) = way_stations.get(&dn.id) else {
                            continue;
                        };

                        let station = Station {
                            id: format!("w{}", id),
                            station: station.clone(),
                            location: (data.lat(dn.lat), data.lon(dn.lon)),
                        };
                        stations.lock().unwrap().push(station);
                    }
                }
                DataEntry::Node(_) => todo!(),
                DataEntry::Way(_) | DataEntry::Relation(_) | DataEntry::ChangeSet(_) => {}
            }
        }

        Ok(())
    })?;

    println!("uploading");

    let mut body = Vec::new();
    for station in stations.into_inner()? {
        if !body.is_empty() {
            body.push(b'\n');
        }
        serde_json::to_writer(&mut body, &station)?;
    }

    ureq::post(&format!(
        "{url}/collections/{collection_name}/documents/import?action=upsert"
    ))
    .set("X-Typesense-Api-Key", &key)
    .send_bytes(&body)?;

    println!("finished");

    Ok(())
}

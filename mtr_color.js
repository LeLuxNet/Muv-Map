// https://zh.wikipedia.org/wiki/Template:%E6%B8%AF%E9%90%B5%E8%BB%8A%E7%AB%99%E9%A1%8F%E8%89%B2/doc

function toRgb(s) {
	const ls = s.toLowerCase();
	return `Rgb::new(0x${ls.slice(1, 3)}, 0x${ls.slice(3, 5)}, 0x${ls.slice(5, 7)})`;
}

const colors = new Map($$(".wikitable tr:has(td)").map(e => {
	const codes = Array.from(e.querySelectorAll("code"));
	const i = codes.findIndex(c => c.innerText.startsWith("#"));
	return [
		codes[0].innerText,
		{ bg: codes[i].innerText, text: codes[i + 1].innerText },
	];
}));

copy(colors
	.entries()
	.map(([code, { bg, text }]) => `"${code}" => (${toRgb(bg)}, ${toRgb(text)}),`)
	.toArray()
	.sort()
	.join("\n"))

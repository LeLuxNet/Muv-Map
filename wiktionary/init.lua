local self_path = debug.getinfo(1).source:match('@?(.*/)')
local scribuntu_path = self_path .. 'Scribunto/includes/Engines/LuaCommon/lualib/'
package.path = scribuntu_path .. '?/?.lua;' .. scribuntu_path .. '?.lua;' .. package.path

local function shallow_copy(t)
	local t2 = {}
	for k, v in pairs(t) do
		t2[k] = v
	end
	return t2
end

local env = shallow_copy(_G)

local real_require = require
function env.require(name)
	local mod = package.loaded[name]
	if mod ~= nil then
		return mod
	end

	-- print('require:' .. name)

	local mod_name = name:match('^Module:(.+)$')
	if mod_name ~= nil then
		local lines = io.lines(self_path .. 'Module/' .. mod_name .. '.txt')
		local function chunk()
			local line = lines()
			if line == nil then
				return nil
			else
				return string.gsub(line .. '\n', 'function ([^(]*)%(([^)]*)%.%.%.%)',
					'function %1(%2...) local arg = table.pack(...)')
			end
		end

		mod = assert(load(chunk, name, 't', env))
		mod = mod()

		package.loaded[name] = mod
		return mod
	else
		return real_require(name)
	end
end

local function load_mw(name, options)
	local mod = assert(loadfile(scribuntu_path .. name .. '.lua', 't', env))()
	if options ~= nil then
		mod.setupInterface(options)
	end
	return mod
end

env.require 'ustring/normalization-data'
env.require 'ustring/charsets'
env.require 'ustring/lower'
env.require 'ustring/upper'
ustring = require 'ustring'
env.require 'ustring/string'

env.mw = load_mw 'mwInit'
env.mw.loadData = env.require
env.mw.hash = load_mw 'mw.hash'
env.mw.html = load_mw 'mw.html'
env.mw.language = load_mw 'mw.language'
env.mw.message = load_mw 'mw.message'
env.mw.site = load_mw 'mw.site'
env.mw.text = load_mw 'mw.text'
env.mw.uri = load_mw 'mw.uri'
env.mw.ustring = ustring
env.mw.title = {
	getCurrentTitle = function()
		return 'abc'
	end
}

function env.mw.text.unstrip(s)
	return s
end

local function loader(name)
	-- print('loader:' .. name)
	return nil -- TODO
end
package.loaders = { nil, loader }

return env

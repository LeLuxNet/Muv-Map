use std::{
	collections::{HashMap, hash_map::Entry},
	fmt::{self, Debug, Formatter},
	io::{self},
	marker::PhantomData,
};

use csv::Reader;
use muv_osm::FromOsmStr;
use serde::{
	Deserialize, Deserializer, Serialize, Serializer,
	de::{self, Unexpected, Visitor, value::StrDeserializer},
};

#[derive(Debug, Clone, Copy)]
pub enum Value<T> {
	Custom(T),
	No,
	Unknown,
}

impl<T> Value<T> {
	pub fn custom(self) -> Option<T> {
		match self {
			Self::Custom(v) => Some(v),
			Self::No | Self::Unknown => None,
		}
	}

	pub fn unwrap(self) -> T {
		match self {
			Self::Custom(v) => v,
			Self::No => panic!("called `Value::unwrap()` on an `No` value"),
			Self::Unknown => panic!("called `Value::unwrap()` on an `Unknown` value"),
		}
	}

	pub const fn is_custom(&self) -> bool {
		matches!(self, Self::Custom(_))
	}
	pub const fn is_no(&self) -> bool {
		matches!(self, Self::No)
	}
	pub const fn is_unknown(&self) -> bool {
		matches!(self, Self::Unknown)
	}
}

impl<T: PartialEq> PartialEq for Value<T> {
	fn eq(&self, other: &Self) -> bool {
		match (self, other) {
			(Self::Custom(lhs), Self::Custom(rhs)) => lhs.eq(rhs),
			(_, _) => false,
		}
	}
}

struct ValueVisitor<T>(PhantomData<T>);

impl<'de, T: Deserialize<'de>> Visitor<'_> for ValueVisitor<T> {
	type Value = Value<T>;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a wikidata value url")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		let Some(path) = v.strip_prefix("http://www.wikidata.org/") else {
			return Err(de::Error::invalid_value(
				Unexpected::Str(v),
				&"to start with http://www.wikidata.org/",
			));
		};

		if path.starts_with(".well-known/genid/") {
			return Ok(Value::Unknown);
		} else if path.starts_with("prop/novalue/") {
			return Ok(Value::No);
		}

		let Some(id_s) = path.strip_prefix("entity/") else {
			return Err(de::Error::invalid_value(
				Unexpected::Str(v),
				&"to start with http://www.wikidata.org/entity/",
			));
		};

		T::deserialize(StrDeserializer::new(id_s)).map(Value::Custom)
	}
}

impl<'de, T: Deserialize<'de>> Deserialize<'de> for Value<T> {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_str(ValueVisitor(PhantomData))
	}
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Q(pub u64);

impl Debug for Q {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "Q{}", self.0)
	}
}

impl FromOsmStr for Q {
	fn from_osm_str(s: &str) -> Option<Self> {
		let num_s = s.strip_prefix('Q')?;
		let num = num_s.parse().ok()?;
		Some(Self(num))
	}
}

impl Serialize for Q {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		format!("Q{}", self.0).serialize(serializer)
	}
}

struct IdVisitor(char);

impl Visitor<'_> for IdVisitor {
	type Value = u64;

	fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("a wikidata id url")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		let Some(num_s) = v.strip_prefix(self.0) else {
			return Err(de::Error::invalid_value(
				Unexpected::Str(v),
				&&*format!("entity id to start with {:?}", self.0),
			));
		};

		let Ok(num) = num_s.parse() else {
			return Err(de::Error::invalid_value(
				Unexpected::Str(v),
				&"to be a number",
			));
		};

		Ok(num)
	}
}

impl<'de> Deserialize<'de> for Q {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_str(IdVisitor('Q')).map(Q)
	}
}

#[derive(Debug, Deserialize)]
pub struct Station {
	pub id: Value<Q>,

	pub owner: Option<Value<Q>>,
	pub operator: Option<Value<Q>>,
	pub network: Option<Value<Q>>,

	pub class: Option<Value<Q>>,

	pub uic_code: Option<String>,
	pub ibnr_id: Option<String>,

	pub uk_code: Option<String>,
	pub db_code: Option<String>,
	pub db_number: Option<String>,
	pub db_number2: Option<String>,
	pub db_number3: Option<String>,
	pub sncf_id: Option<String>,
	pub sncf_gec_id: Option<String>,
	pub rfi_id: Option<String>,
	pub adif_id: Option<String>,
	pub ir_code: Option<String>,
	pub pr_code: Option<String>,
	pub cr_code: Option<String>,
	pub mtr_code: Option<String>,
	pub jr_telegram_code: Option<String>,
}

impl Station {
	pub fn db_number(&self) -> Option<&str> {
		self.db_number
			.as_deref()
			.or(self.db_number2.as_deref())
			.or(self.db_number3.as_deref())
	}

	pub fn class_number(&self) -> Option<u8> {
		Some(match self.class?.unwrap() {
			// China (Q30949085)
			Q(3183365) => 0, // top class
			Q(10864029) => 1,
			Q(10880310) => 2,
			Q(10866573) => 3,
			Q(10925238) => 4,
			Q(10882065) => 5,

			// Taiwan (Q56148526)
			Q(12820822) => 0, // special
			Q(10864031) => 1,
			Q(10880311) => 2,
			Q(10866574) => 3,
			Q(15907831) => 4, // easy
			Q(11077284) => 5, // calling class

			// Germany (Q550637)
			Q(18681579) => 1,
			Q(18681660) => 2,
			Q(18681688) => 3,
			Q(18681690) => 4,
			Q(18681691) => 5,
			Q(18681692) => 6,
			Q(18681693) => 7,
			// AVG (Q129675144)
			Q(129675088) => 1,
			Q(129675089) => 2,

			// Italy (Q56194580)
			Q(56195103) => 1, // platinum
			Q(56195104) => 2, // gold
			Q(56195105) => 3, // silver
			Q(56195107) => 4, // bronze

			// United Kingdom (Q7888186)
			Q(54883424) => 1, // A
			Q(54883641) => 2, // B
			Q(54883692) => 3, // C
			Q(54883781) => 3, // C1
			Q(54883829) => 3, // C2
			Q(54883881) => 4, // D
			Q(54883938) => 5, // E
			Q(54884015) => 6, // F1
			Q(54884061) => 7, // F2

			// Hungary (Q113027198)
			Q(113027199) => 1, // passenger
			Q(113027200) => 2, // passenger
			Q(113027201) => 3, // passenger
			Q(113027202) => 4, // passenger
			Q(113027203) => 1, // freight
			Q(113027204) => 2, // freight
			Q(113027205) => 3, // freight

			class => {
				eprintln!("unknown station class {class:?} for {:?}", self.id.unwrap());
				return None;
			}
		})
	}

	fn merge(&mut self, s: Station) {
		self.owner = self.owner.or(s.owner);
		self.operator = self.operator.or(s.operator);
		self.network = self.network.or(s.network);

		self.class = self.class.or(s.class);

		if self.uic_code.is_none() {
			self.uic_code = s.uic_code;
		}
		if self.ibnr_id.is_none() {
			self.ibnr_id = s.ibnr_id;
		}

		if self.uk_code.is_none() {
			self.uk_code = s.uk_code;
		}
		if self.db_code.is_none() {
			self.db_code = s.db_code;
		}
		if self.db_number.is_none() {
			self.db_number = s.db_number;
		}
		if self.db_number2.is_none() {
			self.db_number2 = s.db_number2;
		}
		if self.db_number3.is_none() {
			self.db_number3 = s.db_number3;
		}
		if self.sncf_id.is_none() {
			self.sncf_id = s.sncf_id;
		}
		if self.sncf_gec_id.is_none() {
			self.sncf_gec_id = s.sncf_gec_id;
		}
		if self.rfi_id.is_none() {
			self.rfi_id = s.rfi_id;
		}
		if self.adif_id.is_none() {
			self.adif_id = s.adif_id;
		}
		if self.ir_code.is_none() {
			self.ir_code = s.ir_code;
		}
		if self.pr_code.is_none() {
			self.pr_code = s.pr_code;
		}
		if self.cr_code.is_none() {
			self.cr_code = s.cr_code;
		}
		if self.mtr_code.is_none() {
			self.mtr_code = s.mtr_code;
		}
		if self.jr_telegram_code.is_none() {
			self.jr_telegram_code = s.jr_telegram_code;
		}
	}
}

pub struct Output {
	pub stations: HashMap<Q, Station>,
}

pub fn read_stations() -> io::Result<Output> {
	let mut r = Reader::from_path("../station_refs.csv")?;

	let mut stations: HashMap<Q, Station> = HashMap::new();
	for d in r.deserialize() {
		let new_s: Station = d?;
		match stations.entry(new_s.id.unwrap()) {
			Entry::Occupied(mut e) => e.get_mut().merge(new_s),
			Entry::Vacant(e) => {
				e.insert(new_s);
			}
		}
	}

	Ok(Output { stations })
}

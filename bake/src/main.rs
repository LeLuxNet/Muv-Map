use std::{
	env::{self, args},
	fmt::{self, Display, Formatter},
	fs::{File, read},
	io::{self, BufReader, BufWriter, Write},
	process::exit,
	result,
	time::{self, Instant},
};

use country_boundaries::{BOUNDARIES_ODBL_360X180, CountryBoundaries};
use muv_osm::{Location, Rgb};
use muv_osm_pbf::{Loader, Result};
use scribuntor::Scribuntor;
use serde::{Serialize, Serializer};
use serde_json::ser::PrettyFormatter;

mod langs;
pub use langs::*;

mod search;
pub use search::*;

mod operator;
pub use operator::*;

mod mtr;

mod map;

mod wikidata;
use wikidata::Q;

mod stage1;
mod stage2;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum ElementId {
	Node(u64),
	Way(u64),
	Relation(u64),
}

impl Serialize for ElementId {
	fn serialize<S: Serializer>(&self, serializer: S) -> result::Result<S::Ok, S::Error> {
		match self {
			Self::Node(id) => format!("n{id}"),
			Self::Way(id) => format!("w{id}"),
			Self::Relation(id) => format!("r{id}"),
		}
		.serialize(serializer)
	}
}

struct Duration(time::Duration);

impl Display for Duration {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		let mut secs = self.0.as_secs();
		let mins = secs / 60;
		if mins > 0 {
			secs -= mins * 60;
			write!(f, "{mins}m")?;
		}
		write!(f, "{secs}.{:03}s", self.0.subsec_millis())
	}
}

impl Duration {
	pub fn since(t: Instant) -> Self {
		Self(t.elapsed())
	}
}

#[derive(Debug, Serialize)]
pub struct Station {
	id: ElementId,
	location: Option<Location>,

	usage: Usage,
	r#type: StationType,
	class: Option<u8>,
	operator: Option<Operator>,
	color: Option<Rgb>,
	text_color: Option<Rgb>,

	wikidata: Option<Q>,
	r#ref: Option<String>,
	web_ref: Option<String>,

	name: Option<String>,
	name_en: Option<String>,
}

#[derive(Debug, Serialize)]
pub enum StationType {
	Rail,
	Tram,
	Subway,
}

#[derive(Debug, Serialize)]
pub enum Usage {
	Passenger,
	Freight,
	Operational,
}

#[derive(Debug)]
struct Platform {}

fn main() -> Result<()> {
	let Some(path) = args().nth(1) else {
		eprintln!("usage: <pbf file>");
		exit(1);
	};

	let wikidata_output = wikidata::read_stations().unwrap();

	let mut l = Loader::new(BufReader::new(File::open(path).unwrap()));

	let now = Instant::now();
	let stage1::Output {
		mut stations,
		station_nodes,
		platforms,
	} = stage1::run(&mut l, &wikidata_output)?;
	println!("collect stations in {}", Duration::since(now));

	let now = Instant::now();
	let stage2::Output { station_areas } =
		stage2::run(&mut l, &mut stations, &station_nodes, &platforms)?;
	println!("collect station areas in {}", Duration::since(now));

	let now = Instant::now();

	let boundaries = CountryBoundaries::from_reader(BOUNDARIES_ODBL_360X180).unwrap();

	let s = Scribuntor::new(|name| Some(read(format!("../wiktionary/Module/{name}.txt")).unwrap()));
	let mut tl = Transliterate::new(&boundaries, &s);
	tl.add("bg", "bg");
	tl.add("el", "gr");
	tl.add("ru", "ru");
	tl.add("uk", "ua");
	tl.add("ko", "kr");
	tl.add("ko", "kp");
	tl.add("kk", "kz");
	tl.add("zh", "cn");

	for s in stations.values_mut() {
		let Some(name) = &s.name else {
			continue;
		};

		if s.name_en.is_none() {
			s.name_en = tl.to_en(name, s.location.unwrap());
		}
	}

	println!("transliterate station names in {}", Duration::since(now));

	let now = Instant::now();

	let f = File::create("../bake.pmtiles").unwrap();
	map::store(f, stations.values());

	println!("render map in {}", Duration::since(now));

	let now = Instant::now();

	store_lua(station_areas.iter().copied()).unwrap();

	let f = BufWriter::new(File::create("stations.json").unwrap());
	let mut ser = serde_json::Serializer::with_formatter(f, PrettyFormatter::with_indent(b"\t"));
	stations.serialize(&mut ser).unwrap();

	if let Ok(typesense_api_key) = env::var("TYPESENSE_API_KEY") {
		let ts = Typesense {
			base_url: "https://typesense.lelux.net/api".into(),
			api_key: typesense_api_key,

			stations_collection: "muv-stations".into(),
		};

		ts.insert_stations(stations.values()).unwrap();
	} else {
		println!("typesense api key not set: skip");
	}

	println!("store results in {}", Duration::since(now));

	Ok(())
}

fn store_lua(station_areas: impl IntoIterator<Item = u64>) -> io::Result<()> {
	let mut f = BufWriter::new(File::create("../bake.lua")?);
	f.write_all(b"local p = {}\n\n")?;

	f.write_all(b"p.station_areas = {")?;
	for id in station_areas {
		writeln!(f, "\t['{id}'] = true,")?;
	}
	f.write_all(b"}\n")?;

	f.write_all(b"\nreturn p")?;

	Ok(())
}

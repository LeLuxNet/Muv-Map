use std::{
	collections::HashMap,
	io::{Read, Seek},
	sync::Mutex,
};

use muv_osm::{FromOsmStr, Location};
use muv_osm_pbf::{Loader, Result, data::Data};

use crate::{
	ElementId, Operator, Platform, Station, StationType, Usage, mtr,
	wikidata::{self, Q},
};

#[derive(Clone, Copy)]
struct Strings {
	railway: u32,
	station: Option<u32>,
	halt: Option<u32>,
	tram_stop: Option<u32>,
	yard: Option<u32>,
	tram: Option<u32>,
	light_rail: Option<u32>,
	subway: Option<u32>,
	platform: Option<u32>,
	railway_ref: Option<u32>,
	ref_crs: Option<u32>,
	r#ref: Option<u32>,
	ref_bvg: Option<u32>,
	ref_jnv: Option<u32>,
	local_ref: Option<u32>,
	wikidata: Option<u32>,
	operator: Option<u32>,
	operator_wikidata: Option<u32>,
	network: Option<u32>,
	network_wikidata: Option<u32>,
	name: Option<u32>,
	name_en: Option<u32>,
}

fn station(
	id: ElementId,
	loc: Option<(i64, i64)>,
	d: Data,
	strings: Strings,
	keys_vals: impl Iterator<Item = Result<(u32, u32)>>,
	wikidata_stations: &wikidata::Output,
) -> Result<Option<Station>> {
	let mut usage = None;
	let mut r#type = StationType::Rail;
	let mut railway_ref_i = None;
	let mut ref_crs_i = None;
	let mut r#ref_i = None;
	let mut ref_bvg_i = None;
	let mut ref_jnv_i = None;
	let mut operator_i = None;
	let mut operator_wikidata_i = None;
	let mut network_i = None;
	let mut network_wikidata_i = None;
	let mut wikidata_i = None;
	let mut name_i = None;
	let mut name_en_i = None;

	for kv in keys_vals {
		let (k, v) = kv?;
		if k == strings.railway {
			if Some(v) == strings.station || Some(v) == strings.halt {
				usage = Some(Usage::Passenger);
			} else if Some(v) == strings.tram_stop {
				usage = Some(Usage::Passenger);
				r#type = StationType::Tram;
			} else if Some(v) == strings.yard {
				usage = Some(Usage::Freight);
			}
		} else if Some(k) == strings.station {
			if Some(v) == strings.tram || Some(v) == strings.light_rail {
				r#type = StationType::Tram;
			} else if Some(v) == strings.subway {
				r#type = StationType::Subway;
			}
		} else if Some(k) == strings.railway_ref {
			railway_ref_i = Some(v);
		} else if Some(k) == strings.ref_crs {
			ref_crs_i = Some(v);
		} else if Some(k) == strings.r#ref {
			ref_i = Some(v);
		} else if Some(k) == strings.ref_bvg {
			ref_bvg_i = Some(v);
		} else if Some(k) == strings.ref_jnv {
			ref_jnv_i = Some(v);
		} else if Some(k) == strings.operator {
			operator_i = Some(v);
		} else if Some(k) == strings.operator_wikidata {
			operator_wikidata_i = Some(v);
		} else if Some(k) == strings.network {
			network_i = Some(v);
		} else if Some(k) == strings.network_wikidata {
			network_wikidata_i = Some(v);
		} else if Some(k) == strings.wikidata {
			wikidata_i = Some(v);
		} else if Some(k) == strings.name {
			name_i = Some(v);
		} else if Some(k) == strings.name_en {
			name_en_i = Some(v);
		}
	}

	let Some(usage) = usage else {
		return Ok(None);
	};

	let location = loc.map(|(lat, lon)| Location::new(d.lat(lat), d.lon(lon)));

	let mut railway_ref = None;
	let mut ref_crs = None;
	let mut r#ref = None;
	let mut ref_bvg = None;
	let mut ref_jnv = None;
	let mut operator = None;
	let mut operator_wikidata = None;
	let mut network = None;
	let mut network_wikidata = None;
	let mut wikidata = None;
	let mut name = None;
	let mut name_en = None;

	let max_i = [
		railway_ref_i,
		ref_crs_i,
		r#ref_i,
		ref_bvg_i,
		ref_jnv_i,
		operator_i,
		operator_wikidata_i,
		network_i,
		network_wikidata_i,
		wikidata_i,
		name_i,
		name_en_i,
	]
	.into_iter()
	.map(|n| n.unwrap_or_default())
	.max()
	.unwrap_or_default();

	for (i, s) in (0..)
		.zip(d.string_table)
		.take(usize::try_from(max_i).unwrap() + 1)
	{
		let s = s?;
		if Some(i) == railway_ref_i {
			railway_ref = Some(s.to_owned());
		}
		if Some(i) == ref_crs_i {
			ref_crs = Some(s.to_owned());
		}
		if Some(i) == ref_i {
			r#ref = Some(s.to_owned());
		}
		if Some(i) == ref_bvg_i {
			ref_bvg = Some(s.to_owned());
		}
		if Some(i) == ref_jnv_i {
			ref_jnv = Some(s.to_owned());
		}
		if Some(i) == operator_i {
			operator = Operator::from_osm_str(s);
		}
		if Some(i) == operator_wikidata_i {
			operator_wikidata = Q::from_osm_str(s).and_then(|q| Operator::try_from(q).ok());
		}
		if Some(i) == network_i {
			network = Operator::from_osm_str(s);
		}
		if Some(i) == network_wikidata_i {
			network_wikidata = Q::from_osm_str(s).and_then(|q| Operator::try_from(q).ok());
		}
		if Some(i) == wikidata_i {
			wikidata = Q::from_osm_str(s)
		}
		if Some(i) == name_i {
			name = Some(s.to_owned());
		}
		if Some(i) == name_en_i {
			name_en = Some(s.to_owned());
		}
	}

	let mut r#ref = railway_ref.or(ref_crs).or(r#ref);
	let mut web_ref = None;

	let mut class = None;
	let mut operator = operator
		.or(operator_wikidata)
		.or(network)
		.or(network_wikidata);

	let wd_station = if let Some(wikidata) = wikidata {
		wikidata_stations.stations.get(&wikidata)
	} else {
		None
	};
	if let Some(wd_station) = wd_station {
		class = wd_station.class_number();

		operator = operator
			.or_else(|| Operator::try_from(wd_station.owner?.unwrap()).ok())
			.or_else(|| Operator::try_from(wd_station.operator?.unwrap()).ok())
			.or_else(|| Operator::try_from(wd_station.network?.unwrap()).ok());

		let (new_ref, new_web_ref) = match operator {
			Some(Operator::NationalRail) => (wd_station.uk_code.as_deref(), None),
			Some(Operator::Db) => (
				wd_station.db_code.as_deref(),
				wd_station.db_number().map(String::from),
			),
			Some(Operator::Sncf) => (
				wd_station
					.sncf_id
					.as_deref()
					.and_then(|id| id.strip_prefix("FR")),
				wd_station.sncf_gec_id.clone(),
			),
			Some(Operator::Rfi) => (None, wd_station.rfi_id.clone()),
			Some(Operator::Adif) => (None, wd_station.adif_id.clone()),
			Some(Operator::Cd | Operator::Mav) => (None, wd_station.uic_code.clone()),
			Some(Operator::Uz) => (
				None,
				wd_station
					.uic_code
					.as_deref()
					.and_then(|uic| uic.get(uic.len().checked_sub(3)?..).map(String::from)),
			),
			Some(Operator::Sbb) => (
				None,
				wd_station
					.uic_code
					.as_deref()
					.and_then(|uic| uic.strip_prefix("85"))
					.map(|uic| uic.trim_start_matches('0'))
					.map(String::from),
			),
			Some(Operator::Ir) => (wd_station.ir_code.as_deref(), None),
			Some(Operator::Pr) => (wd_station.pr_code.as_deref(), None),
			Some(Operator::Mtr) => (wd_station.mtr_code.as_deref(), None),
			_ => (wd_station.cr_code.as_deref(), None),
		};

		r#ref = r#ref.or_else(|| Some(new_ref?.to_owned()));
		web_ref = new_web_ref;
	}

	let r#ref = r#ref.or(ref_bvg).or(ref_jnv);

	let color = r#ref
		.as_deref()
		.filter(|_| operator == Some(Operator::Mtr))
		.and_then(mtr::color);

	let station = Station {
		id,
		location,

		usage,
		r#type,
		class,
		operator,
		wikidata,
		color: color.map(|(bg, _)| bg),
		text_color: color.map(|(_, text)| text),

		r#ref,
		web_ref,

		name,
		name_en,
	};

	Ok(Some(station))
}

pub struct Output {
	pub stations: HashMap<ElementId, Station>,
	pub station_nodes: HashMap<u64, Vec<ElementId>>,
	pub platforms: HashMap<ElementId, Platform>,
}

pub fn run<R: Read + Seek + Send>(
	l: &mut Loader<R>,
	wikidata_stations: &wikidata::Output,
) -> Result<Output> {
	let stations: Mutex<HashMap<ElementId, Station>> = Mutex::default();
	let station_nodes: Mutex<HashMap<u64, Vec<ElementId>>> = Mutex::default();
	let platforms: Mutex<HashMap<ElementId, Platform>> = Mutex::default();

	l.load(|d| {
		let mut railway = None;
		let mut station = None;
		let mut halt = None;
		let mut tram_stop = None;
		let mut yard = None;
		let mut tram = None;
		let mut light_rail = None;
		let mut subway = None;
		let mut platform = None;
		let mut railway_ref = None;
		let mut ref_crs = None;
		let mut r#ref = None;
		let mut ref_bvg = None;
		let mut ref_jnv = None;
		let mut local_ref = None;
		let mut operator = None;
		let mut operator_wikidata = None;
		let mut network = None;
		let mut network_wikidata = None;
		let mut wikidata = None;
		let mut name = None;
		let mut name_en = None;

		for (i, s) in (0u32..).zip(d.string_table.clone()) {
			match s? {
				"railway" => railway = Some(i),
				"station" => station = Some(i),
				"halt" => halt = Some(i),
				"tram_stop" => tram_stop = Some(i),
				"yard" => yard = Some(i),
				"tram" => tram = Some(i),
				"light_rail" => light_rail = Some(i),
				"subway" => subway = Some(i),
				"platform" => platform = Some(i),
				"railway:ref" => railway_ref = Some(i),
				"ref:crs" => ref_crs = Some(i),
				"ref" => r#ref = Some(i),
				"ref:BVG" => ref_bvg = Some(i),
				"ref:JNV" => ref_jnv = Some(i),
				"local_ref" => local_ref = Some(i),
				"operator" => operator = Some(i),
				"operator:wikidata" => operator_wikidata = Some(i),
				"network" => network = Some(i),
				"network:wikidata" => network_wikidata = Some(i),
				"wikidata" => wikidata = Some(i),
				"name" => name = Some(i),
				"name:en" => name_en = Some(i),
				_ => {}
			}
		}

		Ok(railway.map(|railway| Strings {
			railway,
			station,
			halt,
			tram_stop,
			yard,
			tram,
			light_rail,
			subway,
			platform,
			railway_ref,
			ref_crs,
			r#ref,
			ref_bvg,
			ref_jnv,
			local_ref,
			wikidata,
			operator,
			operator_wikidata,
			network,
			network_wikidata,
			name,
			name_en,
		}))
	})
	.dense_node(|d, strings, n| {
		let id = ElementId::Node(n.id);
		if let Some(station) = station(id, Some((n.lat, n.lon)), d, *strings, n, wikidata_stations)?
		{
			stations.lock().unwrap().insert(id, station);
		}

		Ok(())
	})
	.way(|d, strings, mut w| {
		let id = ElementId::Way(w.id);

		if let Some(platform_s) = strings.platform {
			for kv in w.keys_vals.clone() {
				let (k, v) = kv?;
				if k == strings.railway && v == platform_s {
					let platform = Platform {};
					platforms.lock().unwrap().insert(id, platform);
					break;
				}
			}
		}

		if let Some(station) = station(
			id,
			None,
			d.clone(),
			*strings,
			w.keys_vals,
			wikidata_stations,
		)? {
			stations.lock().unwrap().insert(id, station);
			let node = w.refs.next().unwrap()?;
			station_nodes
				.lock()
				.unwrap()
				.entry(node)
				.or_default()
				.push(id);
		}

		Ok(())
	})
	.run()?;

	Ok(Output {
		stations: stations.into_inner().unwrap(),
		station_nodes: station_nodes.into_inner().unwrap(),
		platforms: platforms.into_inner().unwrap(),
	})
}

use std::collections::HashMap;

use country_boundaries::{CountryBoundaries, LatLon};
use mlua::{Function, Table};
use muv_osm::Location;
use scribuntor::{
	Scribuntor,
	wiktionary::{Language, Languages},
};

pub struct Transliterate<'a> {
	boundaries: &'a CountryBoundaries,
	langs: Languages,
	lang: HashMap<String, Language>,
	zh_new_pytemp: Function,
}

impl<'a> Transliterate<'a> {
	pub fn new(boundaries: &'a CountryBoundaries, s: &Scribuntor) -> Self {
		let zh_new: Table = s.module("zh-new").unwrap();
		Self {
			boundaries,
			langs: Languages::new(s),
			lang: HashMap::new(),
			zh_new_pytemp: zh_new.get("pytemp").unwrap(),
		}
	}

	pub fn add(&mut self, lang_code: &str, country_code: &str) {
		let lang = self.langs.by_code(lang_code).unwrap();
		let has_translit = lang.transliterate("", None).unwrap().is_some();
		assert!(has_translit, "language {lang_code} missing translit");

		self.lang.insert(country_code.to_ascii_uppercase(), lang);
	}

	pub fn to_en(&mut self, text: &str, loc: Location) -> Option<String> {
		let lat_lon = LatLon::new(loc.lat, loc.lon).unwrap();
		let ids = self.boundaries.ids(lat_lon);
		for id in ids {
			let Some(lang) = self.lang.get(id) else {
				continue;
			};

			let mut res = match &*lang.code {
				"zh" => self.zh_new_pytemp.call(text).unwrap(),
				_ => {
					let script = lang.find_best_script(text, false);
					if script.code == "None" {
						continue;
					}

					let Ok(res) = lang.transliterate(text, Some(script)) else {
						println!("failed to transliterate {}: {text:?}", lang.code);
						continue;
					};
					res.unwrap()
				}
			};

			if let Some(first) = res.get_mut(..1) {
				first.make_ascii_uppercase();
			}

			return Some(res);
		}
		None
	}
}

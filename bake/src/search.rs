use serde::Serialize;

use crate::ElementId;

pub struct Typesense {
	pub base_url: String,
	pub api_key: String,

	pub stations_collection: String,
}

#[derive(Debug, Serialize)]
struct Station<'a> {
	id: ElementId,
	location: (f64, f64),

	r#ref: Option<&'a str>,
	name: &'a str,
	name_en: Option<&'a str>,
}

impl Typesense {
	fn serialize_stations<'a>(stations: impl IntoIterator<Item = &'a crate::Station>) -> Vec<u8> {
		let mut buf = Vec::new();

		for station in stations {
			let Some(name) = &station.name else {
				continue;
			};

			let loc = station.location.unwrap();
			let ts = Station {
				id: station.id,
				location: (loc.lat, loc.lon),

				r#ref: station.r#ref.as_deref(),
				name,
				name_en: station.name_en.as_deref(),
			};

			serde_json::to_writer(&mut buf, &ts).unwrap();
			buf.push(b'\n');
		}

		buf
	}

	pub fn insert_stations<'a>(
		&self,
		stations: impl IntoIterator<Item = &'a crate::Station>,
	) -> Result<(), ureq::Error> {
		let buf = Self::serialize_stations(stations);

		ureq::post(&format!(
			"{}/collections/{}/documents/import?action=upsert",
			self.base_url, self.stations_collection
		))
		.set("X-Typesense-Api-Key", &self.api_key)
		.send_bytes(&buf)?;

		Ok(())
	}
}

use std::{
	collections::{HashMap, HashSet},
	io::{Read, Seek},
	sync::Mutex,
};

use muv_osm::Location;
use muv_osm_pbf::{Loader, Result, data::MemberType};

use crate::{ElementId, Platform, Station};

pub struct Output {
	pub station_areas: HashSet<u64>,
}

pub fn run<R: Read + Seek + Send>(
	l: &mut Loader<R>,
	stations: &mut HashMap<ElementId, Station>,
	station_nodes: &HashMap<u64, Vec<ElementId>>,
	platforms: &HashMap<ElementId, Platform>,
) -> Result<Output> {
	let is_station: HashSet<ElementId> = stations.keys().copied().collect();
	let stations = Mutex::new(stations);

	let station_areas: Mutex<HashSet<u64>> = Mutex::default();

	l.load(|d| {
		let mut public_transport = None;
		let mut stop_area = None;
		for (i, s) in (0u32..).zip(d.string_table) {
			match s? {
				"public_transport" => public_transport = Some(i),
				"stop_area" => stop_area = Some(i),
				_ => {}
			}
			if public_transport.is_some() && stop_area.is_some() {
				break;
			}
		}

		let rel_data = public_transport.zip(stop_area);
		Ok(Some(rel_data))
	})
	.dense_node(|d, _, n| {
		if let Some(areas) = station_nodes.get(&n.id) {
			let loc = Location::new(d.lat(n.lat), d.lon(n.lon));
			for area in areas {
				stations.lock().unwrap().get_mut(area).unwrap().location = Some(loc);
			}
		}

		Ok(())
	})
	.relation(|_, data, r| {
		let Some((public_transport, stop_area)) = data else {
			return Ok(());
		};

		for kv in r.keys_vals {
			if kv? != (*public_transport, *stop_area) {
				continue;
			}

			let mut has_station = false;
			let mut worthy = false;
			for m in r.members {
				let m = m?;

				// TODO: From
				let id = match m.r#type {
					MemberType::Node => ElementId::Node(m.id),
					MemberType::Way => ElementId::Way(m.id),
					MemberType::Relation => ElementId::Relation(m.id),
				};

				// TODO: derive PartialEq
				if matches!(m.r#type, MemberType::Way) && !platforms.contains_key(&id) {
					worthy = true;
				}

				if is_station.contains(&id) {
					has_station = true;
				}
			}
			if has_station && worthy {
				station_areas.lock().unwrap().insert(r.id);
			}
			break;
		}

		Ok(())
	})
	.run()?;

	Ok(Output {
		station_areas: station_areas.into_inner().unwrap(),
	})
}

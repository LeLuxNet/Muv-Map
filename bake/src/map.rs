use std::{collections::HashMap, io::Write};

use muv_mvt::{LayerWriter, Value};
use muv_osm::{Location, ToOsmStr};
use muv_pmtiles::{Compression, Layer, Metadata, TileId, Writer};

use crate::{Station, StationType};

const HALF_SIZE: f64 = 20_037_508.342_789_248;

fn to_tile_id(loc: Location, size: u32) -> (u32, u32) {
	let (wx, wy) = loc.to_web_mercator();

	let scale = f64::from(size) / 2.0 / HALF_SIZE;
	let x = (HALF_SIZE + wx) * scale;
	let y = (HALF_SIZE - wy) * scale;

	(x as u32, y as u32)
}

fn to_tile_local(loc: Location, z: u8, x: u32, y: u32, extent: u32) -> (u32, u32) {
	let wm = loc.to_web_mercator();
	(
		local_transform(wm.0, z, x, extent),
		local_transform(-wm.1, z, y, extent),
	)
}

fn local_transform(wm: f64, z: u8, tile: u32, extent: u32) -> u32 {
	const EARTH: f64 = 20037508.342789244;
	let zoom_meters = f64::from(1 << z) / (2.0 * EARTH);
	let tile_pos = (EARTH + wm) * zoom_meters - f64::from(tile);
	let scaled_tile_pos = tile_pos * f64::from(extent);
	scaled_tile_pos as u32
}

pub(crate) fn store<'a>(mut f: impl Write, stations: impl Iterator<Item = &'a Station>) {
	let minzoom = 12;
	let maxzoom = 12;
	let extent = 4096;

	let data_size: u32 = 1 << maxzoom;
	let mut data_tiles: HashMap<_, Vec<_>> = HashMap::new();
	for s in stations {
		let loc = s.location.unwrap();
		let (x, y) = to_tile_id(loc, data_size);

		data_tiles.entry(x * data_size + y).or_default().push(s);
	}

	let mut tiles = Writer::default();

	let mut stations_layer_data = Vec::new();

	for z in minzoom..=maxzoom {
		println!("rendering zoom level {z}");
		let z_id = TileId::new(z, 0, 0);

		let size: u64 = 1 << z;
		let cover_size: u32 = 1 << (maxzoom - z);
		for i in 0..size * size {
			let (x, y) = TileId::get_local(z, i);

			stations_layer_data.clear();
			let mut stations_layer = LayerWriter::new(&mut stations_layer_data, "stations", extent);
			let stations_name = stations_layer.key("name");
			let stations_ref = stations_layer.key("ref");
			let stations_web_ref = stations_layer.key("web_ref");
			let stations_class = stations_layer.key("class");
			let stations_text_color = stations_layer.key("text_color");
			let stations_color = stations_layer.key("color");

			for cover_x in x * cover_size..(x + 1) * cover_size {
				for cover_y in y * cover_size..(y + 1) * cover_size {
					for s in data_tiles
						.get(&(cover_x * data_size + cover_y))
						.into_iter()
						.flatten()
					{
						let minzoom = match s.r#type {
							StationType::Tram => 14,
							StationType::Subway => 13,
							StationType::Rail => 12,
						};
						if z < minzoom {
							continue;
						}

						let (x, y) = to_tile_local(s.location.unwrap(), z, x, y, extent);

						let name_id = s.name.as_ref().map(|name| stations_layer.value(name));
						let ref_id = s.r#ref.as_ref().map(|r#ref| stations_layer.value(r#ref));
						let web_ref_id = s
							.web_ref
							.as_ref()
							.map(|web_ref| stations_layer.value(web_ref));
						let class_id = s
							.class
							.map(|class| stations_layer.value(Value::Uint(class.into())));
						let text_color_id = s.text_color.map(|color| {
							stations_layer.value(color.to_osm_str().unwrap().into_owned())
						});
						let color_id = s.color.map(|color| {
							stations_layer.value(color.to_osm_str().unwrap().into_owned())
						});

						let mut station_feature = stations_layer.feature(None);

						if let Some(name) = name_id {
							station_feature.property(stations_name, name);
						}
						if let Some(r#ref) = ref_id {
							station_feature.property(stations_ref, r#ref);
						}
						if let Some(web_ref) = web_ref_id {
							station_feature.property(stations_web_ref, web_ref);
						}
						if let Some(class) = class_id {
							station_feature.property(stations_class, class);
						}
						if let Some(text_color) = text_color_id {
							station_feature.property(stations_text_color, text_color);
						}
						if let Some(color) = color_id {
							station_feature.property(stations_color, color);
						}

						station_feature.point(1).add(x, y);
					}
				}
			}

			drop(stations_layer);

			tiles.add(z_id + i, stations_layer_data.clone());
		}
	}

	let metadata = Metadata {
		vector_layers: vec![Layer {
			id: "stations".into(),
			fields: [
				("name".into(), "String".into()),
				("ref".into(), "String".into()),
			]
			.into_iter()
			.collect(),
			description: None,
			minzoom: None,
			maxzoom: None,
		}],
	};

	tiles
		.finish(&mut f, Compression::Gzip, Compression::Gzip, &metadata)
		.unwrap();
}

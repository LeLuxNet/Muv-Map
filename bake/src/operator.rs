use muv_osm::FromOsmStr;
use serde::Serialize;

use crate::wikidata::Q;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize)]
pub enum Operator {
	Amtrak,
	ViaRail,
	NationalRail,
	Translink,
	Ie,
	Db,
	Sncf,
	Adif,
	Rfi,
	Cd,
	Mav,
	Uz,
	Rzd,
	Cfl,
	Ns,
	Sbb,
	Ir,
	Pr,
	Bvg,
	Jnv,
	Mtr,
	RapidKl,
}

impl TryFrom<Q> for Operator {
	type Error = ();
	fn try_from(value: Q) -> Result<Self, Self::Error> {
		Ok(match value {
			Q(23239) => Self::Amtrak,
			Q(876720) => Self::ViaRail,
			Q(26334) => Self::NationalRail,
			Q(1937120) => Self::Translink,
			Q(73043) => Self::Ie,
			Q(122870674) | Q(1152119) | Q(896765) => Self::Db,
			Q(13646) => Self::Sncf,
			Q(358752) | Q(83819) => Self::Adif,
			Q(1060049) | Q(908916) => Self::Rfi,
			Q(1848025) => Self::Cd,
			Q(168082) => Self::Mav,
			Q(923337) => Self::Uz,
			Q(660770) => Self::Rzd,
			Q(525359) => Self::Cfl,
			Q(23076) => Self::Ns,
			Q(819425) => Self::Ir,
			Q(6867489) | Q(918096) => Self::Pr,
			Q(99633) => Self::Bvg,
			Q(1686811) => Self::Jnv,
			Q(1479375) | Q(14751) => Self::Mtr,
			Q(3271384) => Self::RapidKl,
			_ => return Err(()),
		})
	}
}

impl FromOsmStr for Operator {
	fn from_osm_str(s: &str) -> Option<Self> {
		Some(match s {
			"Amtrak" => Self::Amtrak,
			"VIA Rail Canada" | "VIA Rail" | "Via Rail Canada" | "Via Rail" => Self::ViaRail,
			"National Rail" => Self::NationalRail,
			"Translink NI Railways" => Self::NationalRail,
			"Irish Rail" | "Iarnród Éireann" => Self::Ie,
			"DB InfraGO"
			| "DB InfraGO AG"
			| "DB Station&Service"
			| "DB Station&Service AG"
			| "DB Netz"
			| "DB Netz AG"
			| "DB RegioNetz Infrastruktur GmbH"
			| "Deutsche Bahn"
			| "Deutsche Bahn AG"
			| "DB" => Self::Db,
			"SNCF" => Self::Sncf,
			"Adif" | "ADIF" | "Renfe" => Self::Adif,
			"RFI"
			| "RFI - Rete Ferroviaria Italiana"
			| "Rete Ferroviaria Italiana"
			| "Grandi Stazioni" => Self::Rfi,
			"cz:SŽDC" => Self::Cd,
			"MÁV" => Self::Mav,
			"Укрзалізниця" | "УЗ" => Self::Uz,
			"ОАО «РЖД»" | "РЖД" => Self::Rzd,
			"CFL" => Self::Cfl,
			"Nederlandse Spoorwegen" | "NS" => Self::Ns,
			"SBB" | "CFF" => Self::Sbb,
			"IR" => Self::Ir,
			"Pakistan Railways" | "Pakistan Railway" | "PR" => Self::Pr,
			"Berliner Verkehrsbetriebe" | "Berliner Verkehrsbetriebe (BVG)" => Self::Bvg,
			"JeNah" | "JNV" | "Jenaer Nahverkehrsgesellschaft mbH" => Self::Jnv,
			"香港鐵路有限公司 MTR Corporation Limited"
			| "香港鐵路有限公司 MTR Corporation"
			| "港鐵 MTR"
			| "MTR" => Self::Mtr,
			"Rapid KL" => Self::RapidKl,
			_ => return None,
		})
	}
}

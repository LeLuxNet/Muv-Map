---@meta

---@return string
function Id() end

---@param key string
---@return boolean
function Holds(key) end

---@param key string
---@return string
function Find(key) end

---@return boolean
function HasTags() end

---@param layerName string
---@return string[]
function FindIntersecting(layerName) end

---@param layerName string
---@return number
function AreaIntersecting(layerName) end

---@param layerName string
---@return boolean
function Intersects(layerName) end

---@param layerName string
---@return string[]
function FindCovering(layerName) end

---@param layerName string
---@return boolean
function CoveredBy(layerName) end

---@return boolean
function IsClosed() end

---@return number
function Area() end

---@return number
function Length() end

---@alias CentroidAlgorithm "polylabel" | "centroid"

---@param algorithm? CentroidAlgorithm
---@return { [1]: number, [2]: number }?
function Centroid(algorithm) end

---@param layerName string
---@param area? boolean
function Layer(layerName, area) end

---@param layerName string
---@param algorithm? CentroidAlgorithm
---@param role? string
---@param ... string
function LayerAsCentroid(layerName, algorithm, role, ...) end

---@param key string
---@param val string
---@param minzoom? number
function Attribute(key, val, minzoom) end

---@param key string
---@param val number
---@param minzoom? number
function AttributeNumeric(key, val, minzoom) end

---@param key string
---@param val boolean
---@param minzoom? number
function AttributeBoolean(key, val, minzoom) end

---@param zoom number
function MinZoom(zoom) end

---@param number number
function ZOrder(number) end

---@return integer
function NextRelation() end

function RestartRelations() end

---@param key string
---@return string
function FindInRelation(key) end

function Accept() end

---@param key string
---@param value string
function SetTag(key, value) end

---@type string[]
node_keys = {}

---@type string[]
way_keys = {}

function node_function() end

function way_function() end

---@param name string
function init_function(name) end

function exit_function() end

---@param layer string
function attribute_function(attr, layer) end

function relation_scan_function() end

function relation_postscan_function() end

function relation_function() end
